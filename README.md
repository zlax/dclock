# Erisian digital clock

By default, it shows erisian time (hours:minutes). Pressing the button temporarily changes the display mode to: erisian time (minutes:seconds), date (season and day), day of week, yold.

Used hardware modules: DS1307 Real Time Clock and TM1637 Digit Display

Check timezoneOffset variable for your local timezone.

https://discordia.fandom.com/wiki/Erisian_Time

![dclock](https://dev.ussr.win/zlax/dclock/raw/branch/master/dclock_bb.jpg)
